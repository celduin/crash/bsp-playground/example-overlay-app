#!/bin/sh
# Download an SD card image from Celduin cache
URL=https://celduin-cache.codethink.co.uk:8080/dev/bsp_poc
USERNAME=celduin
OUTPUT=sdcard.zip

usage()
{
	echo "usage: PASSWORD=xxx $0 IMG_TYPE [BRANCH]"
	echo "   eg: PASSWORD=xxx $0 pi"
}

check_env_or_die()
{
	if [ -z "$PASSWORD" ]; then
		echo "PASSWORD is not set"
		usage
		exit 1
	fi
}

# set the args in global scope
get_args_or_die()
{
	if [ "$#" -ge 1 ]; then
		img_type="$1"
	else
		usage
		exit 1
	fi
	if [ $# -ge 2 ]; then
		branch="$2"
	else
		branch="$(git rev-parse --abbrev-ref HEAD)"
	fi
}

check_env_or_die
get_args_or_die "$@"

echo "Using branch '$branch'"
url_sdcard="${URL}/${branch}/${img_type}/$OUTPUT"

echo "Downloading $url_sdcard"
curl \
	--user "$USERNAME:$PASSWORD" \
	--output "$OUTPUT" \
	--insecure \
	"$url_sdcard" && \
unzip "$OUTPUT"
