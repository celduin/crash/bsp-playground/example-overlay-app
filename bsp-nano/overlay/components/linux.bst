kind: manual
description: Linux kernel configured for use in the Jetson Nano

depends:
- filename: bootstrap-import.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: components/bison.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: components/flex.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: components/bc.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: components/gzip.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: components/kmod.bst
  junction: freedesktop-sdk.bst

variables:
  bootdir: /boot

config:
  configure-commands:
  - |
    # Generate the default kernel config for the target architecture
    make tegra_defconfig

  - |
    # Modify the kernel config for additional features

    # Kernel Config Options
    scripts/config -e DEVTMPFS
    scripts/config -e CGROUPS
    scripts/config -e INOTIFY_USER
    scripts/config -e SIGNALFD
    scripts/config -e TIMERFD
    scripts/config -e EPOLL
    scripts/config -e NET
    scripts/config -e SYSFS
    scripts/config -e PROC_FS
    scripts/config -e FHANDLE

    # Kernel crypto/hash API
    scripts/config -e CRYPTO_USER_API_HASH
    scripts/config -e CRYPTO_HMAC
    scripts/config -e CRYPTO_SHA256

    # udev will fail to work with legacy sysfs
    scripts/config -d SYSFS_DEPRECATED

    # Boot is very slow with systemd when legacy PTYs are present
    scripts/config -d LEGACY_PTYS
    scripts/config -d LEGACY_PTY_COUNT

    # Legacy hotplug confuses udev
    scripts/config --set-str UEVENT_HELPER_PATH ""

    # Userspace firmware loading not supported
    scripts/config -d FW_LOADER_USER_HELPER
    scripts/config -d FW_LOADER_USER_HELPER_FALLBACK

    # Some udev/virtualization requires
    scripts/config -e DMIID

    # Support for some SCSI devices serial number retrieval
    scripts/config -e BLK_DEV_BSG

    # Required for PrivateNetwork= in service units
    scripts/config -e NET_NS
    scripts/config -e USER_NS

    # Required for 9p support
    scripts/config -e NET_9P
    scripts/config -e NET_9P_VIRTIO
    scripts/config -e 9P_FS
    scripts/config -e 9P_FS_POSIX_ACL
    scripts/config -e VIRTIO_PCI

    # Strongly Recommended
    scripts/config -e IPV6
    scripts/config -e AUTOFS4_FS
    scripts/config -e TMPFS_XATTR
    scripts/config -e TMPFS_POSIX_ACL
    scripts/config -e EXT4_FS_POSIX_ACL
    scripts/config -e XFS_POSIX_ACL
    scripts/config -e BTRFS_FS_POSIX_ACL
    scripts/config -e SECCOMP
    scripts/config -e SECCOMP_FILTER
    scripts/config -e CHECKPOINT_RESTORE

    # Required for CPUShares= in resource control unit settings
    scripts/config -e CGROUP_SCHED
    scripts/config -e FAIR_GROUP_SCHED

    # Required for CPUQuota= in resource control unit settings
    scripts/config -e CFS_BANDWIDTH

    # Required for IPAddressDeny=, IPAddressAllow= in resource control unit settings
    scripts/config -e CGROUP_BPF

    # For UEFI systems
    scripts/config -e EFIVAR_FS
    scripts/config -e EFI_PARTITION

    # RT group scheduling (effectively) makes RT scheduling unavailable for userspace
    scripts/config -d RT_GROUP_SCHED

    # Required for 3D acceleration in qemu
    scripts/config -e CONFIG_DRM_VIRTIO_GPU

    # Required for systemd-nspawn
    scripts/config -e DEVPTS_MULTIPLE_INSTANCES

  build-commands:
  - |
    make KCFLAGS="-Wno-error=address-of-packed-member -Wno-error=missing-attributes -Wno-all"

  install-commands:
  - |
    install -Dm644 arch/arm64/boot/Image '%{install-root}%{bootdir}/Image'
    install -Dm644 System.map '%{install-root}%{bootdir}/System.map'
    cp arch/arm64/boot/dts/tegra210-* '%{install-root}%{bootdir}/'
    make INSTALL_MOD_PATH='%{install-root}%{prefix}' INSTALL_MOD_STRIP=1 modules_install

    rm %{install-root}%{prefix}/lib/modules/*/{source,build}

public:
  bst:
    integration-commands:
    - |
      cd '%{prefix}/lib/modules'
      for version in *; do
        depmod -b '%{prefix}' -a "$version";
      done

    split-rules:
      devel:
        (>):
        - '%{bootdir}/System.map'

sources:
- kind: git
  url: https://github.com/madisongh/linux-tegra-4.9
  track: patches-l4t-r32.2.1
  ref: 7467bde43b5d678b39e2bd2ed0968bd21cd6eeb1
