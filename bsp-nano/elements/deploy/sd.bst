kind: script
description: |
  Ostree rootfs layout

depends:
- filename: bootstrap-import.bst
  junction: freedesktop-sdk.bst
  type: build
- filename: deploy-tools/L4T.bst
  type: build
- filename: deploy/filesystem.bst
  type: build
- filename: bsp/tegra-uboot.bst
  type: build

variables:
  cwd: '%{build-root}'

  # For OSTree deployment
  OSNAME: "ct-os"
  BOOTLOADER: "uEnv"
  # The ostree remote URL in installed configuration.
  # It should be a valid URL and not this placeholder.
  OSTREE_REMOTE_NAME: "ct-origin"
  OSTREE_REMOTE_URL: "OSTREEREMOTEURL"
  OSTREE_BRANCH: "TRACKEDBRANCH"
  # Create the ostree sysroot
  OSTREE_SYSROOT: '%{install-root}/sysroot'
  OSTREE_SYSROOT_REPO: '%{OSTREE_SYSROOT}/ostree/repo'
  OSTREE_SYSROOT_BOOT: '%{OSTREE_SYSROOT}/boot'

  # We must expand the firmware search path via firmware_class.path
  # because the nvidia modules dont follow symlinks, we may be able to tweak
  # this in the future and remove this argument.
  kernel-args: ${cbootargs} rootfstype=ext4 root=/dev/mmcblk0p13 console=ttyS0,115200n8 rw rootwait firmware_class.path=/usr/lib/firmware net.ifnames=0

  L4T: '%{install-root}/L4T'
  OSTREE_ARTIFACTS: '%{L4T}/artifacts'
  REPO: '%{OSTREE_ARTIFACTS}/repo'

config:
  layout:
  - element: bootstrap-import.bst
    destination: /
  - element: deploy-tools/L4T.bst
    destination: '%{L4T}'
  - element: boards/filesystem.bst
    destination: '%{OSTREE_ARTIFACTS}'
  - element: bsp/tegra-uboot.bst
    destination: /tegra-uboot/

  commands:
   # Deployment

  - mkdir -p "%{OSTREE_SYSROOT}"
  - |
    ostree admin init-fs "%{OSTREE_SYSROOT}"
    ostree admin os-init "%{OSNAME}" --sysroot="%{OSTREE_SYSROOT}"

    # Add a remote to the ostree sysroot, so we can pull on the target
    ostree --repo="%{OSTREE_SYSROOT_REPO}" remote add \
        "%{OSTREE_REMOTE_NAME}" "%{OSTREE_REMOTE_URL}" "%{OSTREE_BRANCH}" \
        --no-gpg-verify --set=tls-permissive=true

  - |
    ostree --repo="%{OSTREE_SYSROOT_REPO}" pull-local \
        "%{REPO}" "%{OSTREE_BRANCH}" --remote="%{OSTREE_REMOTE_NAME}" \
        --disable-fsync

  - |
    case "%{BOOTLOADER}" in
    "syslinux")
        # OSTree will see the syslinux/syslinux.cfg file to
        # recognise the bootloader. An empty file is good
        # enough.
        mkdir -p "%{OSTREE_SYSROOT_BOOT}"/{syslinux,extlinux}
        touch "%{OSTREE_SYSROOT_BOOT}/syslinux/syslinux.cfg"
        ;;
    "uEnv")
        # This is exceptionally hacky, but in order to create a valid uEnv.txt, deploy needs to be called twice.
        # currently ostree requires that /boot/loader is a symlink; https://github.com/ostreedev/ostree/issues/1951
        ostree admin --sysroot="%{OSTREE_SYSROOT}" deploy \
            --os="%{OSNAME}" \
            -v \
            "%{OSTREE_REMOTE_NAME}:%{OSTREE_BRANCH}" \
            --karg-append='%{kernel-args}'
        touch "%{OSTREE_SYSROOT_BOOT}/loader/uEnv.txt"
        ;;
    *)
        echo "Unknown bootloader %{BOOTLOADER}"
        exit 1
    esac

  - |
    # Deploy with root=UUID random
    ostree admin --sysroot="%{OSTREE_SYSROOT}" deploy \
        --os="%{OSNAME}" \
        -v \
        "%{OSTREE_REMOTE_NAME}:%{OSTREE_BRANCH}" \
        --karg-append='%{kernel-args}'

  - |
    case "%{BOOTLOADER}" in
    "syslinux")
        # Create a symlink from syslinux.cfg and extlinux.conf
        # To point at the OSTree-generated one.

        rm "%{OSTREE_SYSROOT_BOOT}/syslinux/syslinux.cfg"

        ln -s ../loader/syslinux.cfg \
            "%{OSTREE_SYSROOT_BOOT}/extlinux/extlinux.conf"

        ln -s ../loader/syslinux.cfg \
            "%{OSTREE_SYSROOT_BOOT}/syslinux/syslinux.cfg"
        ;;

    "uEnv")
        ln -s ./loader/uEnv.txt \
            "%{OSTREE_SYSROOT_BOOT}/uEnv.txt"
        cp /tegra-uboot/uboot/u-boot-preboot.scr "%{OSTREE_SYSROOT_BOOT}/u-boot-preboot.scr"
        ;;
    *)
        echo "Unknown bootloader %{BOOTLOADER}"
        exit 1
    esac

  - |
    # Copy DTB files from our build
    mkdir -p %{L4T}/kernel/dtb
    find %{OSTREE_SYSROOT} -name *dtb -exec cp  {} %{L4T}/kernel/dtb \;

    # Copy kernel file, maybe not needed?
    find %{OSTREE_SYSROOT} -name vmlinuz -exec cp  {} %{L4T}/kernel/Image \;

    # Install u-boot (will work if using syslinux)
    cp /tegra-uboot/uboot/u-boot.bin %{L4T}/bootloader/t210ref/p3450-porg/u-boot.bin
    cp /tegra-uboot/uboot/u-boot.bin %{L4T}/bootloader/boot.img

  # Workaround because we can't use OSTree inside L4T directory
  - ln -s ../sysroot '%{L4T}/sysroot'

  - |
    cat > %{install-root}/create-sd-image.sh << EOF
    #!/bin/sh
    cd L4T
    sudo ROOTFS_DIR=sysroot ./create-jetson-nano-sd-card-image.sh -o ../image.img -s 24G -r 200
    EOF
    chmod +x %{install-root}/create-sd-image.sh
