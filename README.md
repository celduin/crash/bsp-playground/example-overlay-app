BSP Example Overlay App Approach
===============================

This PoC repo aims to show how an example $APP can be built against many
target BSPs, via a single project definition. In this case, a definition for
Vim is overlayed into each target BSP, from the app-project entry point.

The project is based on bst1.4 & buildstream-external plugins, with the aim of transitioning to a point release of BuildStream master.

## Build the Rasberry pi image

``` shell
cd app-project
bst track bsp-raspi.bst
bst build bsp-raspi.bst:deploy/minimal-img.bst
bst checkout bsp-raspi.bst:deploy/minimal-img.bst raspi-img

sudo dd if=raspi-img/sdcard.img of=$SDCARD status=progress bs=1M
```


## Build the qemu image

``` shell
cd app-project
bst track bsp-qemu.bst
bst build bsp-qemu.bst:deploy/minimal-img.bst
bst checkout bsp-qemu.bst:deploy/minimal-img.bst qemu-img
```

### Run the image

Extract vmlinuz from qemu-img/sdcard.img

```shell
extractedVmlinuz="path/to/extracted/vmlinuz" # This is the path to the vmlinuz file exctracted from the sdcard.img
qemu-system-x86_64 -kernel $extractedVmlinuz -nographic -append "console=ttyS0 root=/dev/sda2 init=/bin/sh" -hda qemu-img/sdcard.img
```

## Download these images from the Celduin cache
Sometimes we rely on the GitLab CI to build the above images - especially when
targeting embedded architectures. The final sdcard.zip gets uploaded to a cache
server, which we can access using curl.

Use the helper script to download pi and qemu images like so (defaults to
current branch):

    PASSWORD=xxx scripts/dl-sdcard.sh pi
    PASSWORD=xxx scripts/dl-sdcard.sh qemu
    PASSWORD=xxx scripts/dl-sdcard.sh pi user/feature-branch
